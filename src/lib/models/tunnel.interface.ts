export interface Tunnel {
    name: string;
    url: string;
    port: string;
}