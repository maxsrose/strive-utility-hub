import type {Tunnel} from "$lib/models/tunnel.interface";
import {invoke} from "@tauri-apps/api";
import type {Link} from "$lib/models/links.interface";

export const getLinks = async (): Promise<Link[]> => {
    return invoke("get_links")
}

export const getTunnels = async (): Promise<Set<Tunnel>> => {
    return invoke("get_tunnels");
}

export const startTunnel = async (name: string) => {
    return invoke("start_tunnel", { name });
}

export const stopTunnel = async (name: string) => {
    return invoke("stop_tunnel", { name });
}