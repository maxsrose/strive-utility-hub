import {writable} from "svelte/store";
import type {Settings} from "$lib/models/settings.interface";
import {BaseDirectory, exists, readTextFile, writeTextFile} from "@tauri-apps/api/fs";
import {appWindow} from "@tauri-apps/api/window";

const defaultSettings: Settings = {
    heisenlamb: false,
    dark: false
};

const initSettingsFile = async () => {
    const hasSettings = await exists("custom-settings.json", { dir: BaseDirectory.AppConfig });

    if(hasSettings)
        return;

    const isDark = (await appWindow.theme()) === 'dark'

    await writeTextFile("custom-settings.json", JSON.stringify({...defaultSettings, dark: isDark}), {  dir: BaseDirectory.AppConfig });
}

const getFileContent = async () => {
    await initSettingsFile();

    const content = await readTextFile("custom-settings.json", { dir: BaseDirectory.AppConfig });

    return {...defaultSettings, ...JSON.parse(content)};
}

const {subscribe, update, set} = writable<Settings>(
    defaultSettings,
    () => {
        getFileContent()
            .then(r => {
                set(r);
            });
    });

const updateSettings = async (data: Partial<Settings>) => {
    update((s) => {
        s = {...s, ...data};

        writeTextFile("custom-settings.json", JSON.stringify(s), { dir: BaseDirectory.AppConfig });

        return s;
    });
}

export default {
    subscribe,
    updateSettings
}