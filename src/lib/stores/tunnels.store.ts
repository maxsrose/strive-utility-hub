import {writable} from "svelte/store";

const { subscribe } = writable<Set<string>>(new Set());

export default {
    subscribe
};