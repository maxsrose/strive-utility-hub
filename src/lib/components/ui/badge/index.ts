import {tv, type VariantProps} from "tailwind-variants";

export { default as Badge } from "./badge.svelte";

export const badgeVariants = tv({
	base: "inline-flex select-none items-center rounded-full border px-2.5 py-0.5 text-xs font-semibold transition-colors focus:outline-none focus:ring-2 focus:ring-ring focus:ring-offset-2",
	variants: {
		variant: {
			default: "border-transparent bg-primary text-primary-foreground",
			secondary:
				"border-transparent bg-secondary text-secondary-foreground0",
			destructive:
				"border-transparent bg-destructive text-destructive-foreground",
			pending:
				"border-transparent bg-amber-500 text-destructive-foreground",
			success:
				"border-transparent bg-green-500 text-destructive-foreground",
			outline: "text-foreground",
		},
	},
	defaultVariants: {
		variant: "default",
	},
});

export type Variant = VariantProps<typeof badgeVariants>["variant"];
