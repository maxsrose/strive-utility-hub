// Prevents additional console window on Windows in release, DO NOT REMOVE!!
#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")]

use std::collections::{HashMap, HashSet};
use std::env;
#[cfg(target_os = "windows")]
use std::os::windows::process::CommandExt;
use std::process::{Child, Command};
use std::sync::Mutex;

use config::Config;
#[cfg(not(target_os = "windows"))]
use fix_path_env::fix;
use serde::{Deserialize, Serialize};
use tauri::{
    CustomMenuItem, Manager, SystemTray, SystemTrayEvent, SystemTrayMenu, SystemTrayMenuItem,
    Window,
};
use tauri_plugin_positioner::{Position, WindowExt};

#[derive(Clone, Serialize, Deserialize)]
struct Tunnel {
    name: String,
    url: String,
    port: String,
}

#[derive(Serialize, Deserialize, Clone)]
struct Link {
    name: String,
    url: Option<String>,
    links: Option<Vec<Link>>,
}

#[derive(Deserialize)]
struct AppConfig {
    tunnels: Vec<Tunnel>,
    links: Vec<Link>,
}

#[derive(Default)]
struct App {
    tunnels: Vec<Tunnel>,
    links: Vec<Link>,
    running_tunnel: HashMap<String, Child>,
}

struct AppState(Mutex<App>);

fn main() {
    #[cfg(not(target_os = "windows"))]
    let _ = fix();

    let application = tauri::Builder::default()
        .manage(AppState(Default::default()))
        .setup(|app| {
            let resource_path = app
                .path_resolver()
                .resolve_resource("settings.json")
                .expect("Failed to resolve resource");

            let settings = Config::builder()
                .add_source(config::File::from(resource_path))
                .build()
                .unwrap();
            let settings = settings.try_deserialize::<AppConfig>().unwrap();

            let app = app.state::<AppState>();
            let mut app = app.0.lock().unwrap();
            app.links = settings.links;
            app.tunnels = settings.tunnels;

            Ok(())
        })
        .system_tray(build_system_tray())
        .plugin(tauri_plugin_positioner::init())
        .on_system_tray_event(|app, event| match event {
            SystemTrayEvent::LeftClick { .. } => {
                let mut window = app.get_window("main").unwrap();
                show_window(&mut window);
            }
            SystemTrayEvent::MenuItemClick { id, .. } => match id.as_str() {
                "quit" => {
                    app.get_window("main").unwrap().close().unwrap();
                }
                "hide" => {
                    app.get_window("main").unwrap().hide().unwrap();
                }
                "show" => {
                    let mut window = app.get_window("main").unwrap();
                    show_window(&mut window);
                }
                _ => {}
            },
            _ => {}
        })
        .invoke_handler(tauri::generate_handler![
            get_links,
            get_tunnels,
            get_running_tunnels,
            start_tunnel,
            stop_tunnel
        ])
        .build(tauri::generate_context!())
        .expect("Error whilst building application");

    application.run(|app_handle, event| match event {
        tauri::RunEvent::ExitRequested { .. } => {
            println!("Exit requested");
            let app = app_handle.state::<AppState>();
            let mut app_state = app.0.lock().unwrap();
            let running_tunnels = app_state
                .running_tunnel
                .keys()
                .cloned()
                .collect::<HashSet<String>>();

            running_tunnels.iter().for_each(|k| {
                let mut child = app_state.running_tunnel.remove(k).unwrap();

                child.kill().unwrap();
                child.wait().unwrap();
            });
        }
        _ => {}
    });
}

fn build_system_tray() -> SystemTray {
    let quit = CustomMenuItem::new("quit".to_string(), "Quit");
    let hide = CustomMenuItem::new("hide".to_string(), "Hide");
    let show = CustomMenuItem::new("show".to_string(), "Show");
    let tray_menu = SystemTrayMenu::new()
        .add_item(quit)
        .add_native_item(SystemTrayMenuItem::Separator)
        .add_item(hide)
        .add_item(show);

    SystemTray::new().with_menu(tray_menu)
}

fn show_window(window: &mut Window) {
    window.move_window(Position::BottomRight).unwrap();
    window.show().unwrap();
    window.set_focus().unwrap();
}

#[tauri::command]
fn get_links(app: tauri::State<AppState>) -> Vec<Link> {
    let app = app.0.lock().unwrap();

    app.links.clone()
}

#[tauri::command]
fn get_tunnels(app: tauri::State<AppState>) -> Vec<Tunnel> {
    let app = app.0.lock().unwrap();

    app.tunnels.clone()
}

#[tauri::command]
fn get_running_tunnels(app: tauri::State<AppState>) -> Vec<String> {
    let app = app.0.lock().unwrap();

    app.running_tunnel.keys().cloned().collect::<Vec<_>>()
}

#[tauri::command]
fn start_tunnel(name: String, app: tauri::State<AppState>) -> Result<bool, String> {
    println!("Starting tunnel {}", name);

    let app = app.0.lock();

    if app.is_err() {
        return Err("Failed to get lock on app state".to_string());
    }

    let mut app = app.unwrap();

    if app.running_tunnel.contains_key(&name) {
        println!("Tunnel already running!");
        return Ok(true);
    }

    match app.tunnels.iter().find(|t| t.name == name) {
        Some(tunnel) => {
            let mut child = Command::new("cloudflared");
            let child = child
                .arg("access")
                .arg("tcp")
                .arg(format!("-T={}", tunnel.url))
                .arg(format!("-L=localhost:{}", tunnel.port));

            #[cfg(target_os = "windows")]
                let child = child.creation_flags(0x08000000);

            match child.spawn() {
                Ok(child) => {
                    app.running_tunnel.insert(name, child);

                    Ok(true)
                },
                Err(_) => {
                    Err("Failed to start tunnel".to_string())
                }
            }
        }
        None => Ok(true),
    }
}

#[tauri::command]
fn stop_tunnel(name: String, app: tauri::State<AppState>) -> Result<bool, String> {
    println!("Stopping tunnel {}", name);
    let app = app.0.lock();

    if app.is_err() {
        return Err("Failed to get lock on app state".to_string());
    }

    let mut app = app.unwrap();

    if !app.running_tunnel.contains_key(&name) {
        println!("Nothing to stop");
        return Ok(false);
    }

    match app.running_tunnel.remove(&name).as_mut() {
        Some(child) => {
            match child.kill() {
                Ok(_) => {
                    let _ = child.wait();
                    Ok(true)
                },
                Err(_) => {
                    Err("Failed to stop tunnel".to_string())
                }
            }
        },
        None => Ok(true)
    }
}
